
USE test_db;

CREATE TEMPORARY TABLE pairs (model1 INTEGER,
                              model2 INTEGER);

INSERT INTO pairs

SELECT DISTINCT TMP1.model, TMP2.model

FROM

PC AS TMP1

CROSS JOIN

PC AS TMP2

ON TMP1.model < TMP2.model AND TMP1.ram = TMP2.ram AND TMP1.speed = TMP2.speed;


SELECT * FROM pairs;

