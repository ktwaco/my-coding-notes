
USE test_db;

/* neato !!! */
CREATE TEMPORARY TABLE test LIKE Product;

/* temp tables */
CREATE TEMPORARY TABLE makes_laptops (maker CHAR(1) UNIQUE);
CREATE TEMPORARY TABLE makes_pcs (maker CHAR(1) UNIQUE);

/* get makers of laptops and potentially pcs */
INSERT IGNORE INTO makes_laptops

    SELECT maker

    FROM Product

    WHERE type = 'laptop';

SELECT * FROM makes_laptops;

/* get makers of pcs */
INSERT IGNORE INTO makes_pcs

    SELECT maker

    FROM Product

    WHERE type = 'pc';

SELECT * FROM makes_pcs;

/* get only makers who make laptops NOT PCS */
SELECT DISTINCT makes_laptops.maker

FROM makes_laptops

WHERE makes_laptops.maker NOT IN (SELECT DISTINCT makes_pcs.maker FROM makes_pcs);


